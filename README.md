# Solid Score Recorder Backend

## Running DEV with Docker (compose)

1. Make sure the database urls for `scor_global` and `scor_dart` in the [`dev.env`](./dev.env) file match your local setup and docker configuration
2. Build container with `make build-dev`
3. Run container with `make run-dev`

Now you can visit [`localhost:4000/api/graphiql`](http://localhost:4000/api/graphiql) from your browser.

The container watches for code changes and _should_ recompile on the next request whose execution path hits the changed code. This does *not* happen for changes to the config files, endpoint or router modules, so if in doubt, re-run the container. 

For the actual docker-compose commands, see the [Makefile](./Makefile), to change the host port that the application is bound to, see the [compose file](./dev.docker-compose.yml)

## Running DEV with a local elixir installation
To start your Phoenix server:

  * Install dependencies with `mix deps.get`
  * Start Phoenix endpoint with `mix phx.server` or inside IEx with `iex -S mix phx.server`

Now you can visit [`localhost:4000/api/graphiql`](http://localhost:4000/api/graphiql) from your browser.

The app watches for code changes and _should_ recompile on the next request whose execution path hits the changed code. This does *not* happen for changes to the config files, endpoint or router modules, so if in doubt, re-run the app. 