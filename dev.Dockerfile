FROM elixir:latest

WORKDIR /app

RUN mix local.rebar --force
RUN mix local.hex --force

COPY mix.exs .
COPY mix.lock .

CMD mix deps.get && mix phx.server