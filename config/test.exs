import Config

# Configure your database

# scor_global
fallback_global_url = "postgres://scor_global:scor_global@localhost:5432/scor_global"
database_global_url = System.get_env("DATABASE_GLOBAL_URL") || fallback_global_url
config :recorder, Recorder.GlobalRepo,
  url: database_global_url,
  pool: Ecto.Adapters.SQL.Sandbox,
  pool_size: 10

# scor_dart
fallback_dart_url = "postgres://scor_dart:scor_dart@localhost:5432/scor_dart"
database_dart_url = System.get_env("DATABASE_DART_URL") || fallback_dart_url
config :recorder, Recorder.DartRepo,
  url: database_dart_url,
  pool: Ecto.Adapters.SQL.Sandbox,
  pool_size: 10

# We don't run a server during test. If one is required,
# you can enable the server option below.
config :recorder, RecorderWeb.Endpoint,
  http: [ip: {127, 0, 0, 1}, port: 4002],
  server: false

# Print only warnings and errors during test
config :logger, level: :warn

# Initialize plugs at runtime for faster test compilation
config :phoenix, :plug_init_mode, :runtime
