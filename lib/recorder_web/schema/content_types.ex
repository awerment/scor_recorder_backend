defmodule RecorderWeb.Schema.ContentTypes do
  use Absinthe.Schema.Notation
  import_types Absinthe.Type.Custom

  object :player do
    field :login, :string
    field :uuid, :string
    field :state, :string
    field :pass_number, :string
    field :first_name, :string
    field :last_name, :string
    field :comment, :string
    field :user_ref, :string
    field :nation, :string
    field :date_of_birth, :date
    field :contact_data_ref, :string
    field :created_by, :string
    field :created_date, :date
    field :last_modified_by, :string
    field :last_modified_date, :date
    field :email, :string
  end

  object :game_config do
    field :game_count, :integer
    field :game_distance, :integer
    field :set_distance, :integer
    field :leg_distance, :integer
    field :shortleg, :integer
    field :capture_shortleg, :boolean
    field :capture_highfinish, :boolean
    field :capture_one_eighty, :boolean
    field :capture_one_seventy_one, :boolean
    field :capture_average, :boolean
    field :highfinish, :integer
    field :starting_team, :string
    field :begin_games, :string
  end

  object :disqualification_config do
    field :consider_in_table, :boolean
    field :consider_in_player_statistics, :boolean
    field :consider_in_team_statistics, :boolean
  end

  object :borrow_config do
    field :borrow_max_count, :integer
    field :borrow_direction, :string
    field :borrow_interval, :string
  end

  object :range_config do
    field :upper_bound, :integer
    field :lower_bound, :integer
  end

  object :scoring_config do
    field :is_draw_allowed, :boolean
    field :winning_points, :integer
    field :losing_points, :integer
    field :draw_points, :integer
    field :deciding_match_points, :integer
  end

  object :config do
    field :version, :integer
    field :rounds, :integer
    field :name, :string
    field :use_season_report, :boolean
    field :is_draw_allowed, :boolean
    field :default_match_start_time, :date
    field :single_config, :game_config
    field :double_config, :game_config
    field :disqualify_config, :disqualification_config
    field :borrow_config, :borrow_config
    field :rank_ascent_config, :range_config
    field :rank_descent_config, :range_config
    field :rank_neutral_config, :range_config
    field :relegation_ascent_config, :range_config
    field :relegation_descent_config, :range_config
    field :default_ascent_league_uuid, :string
    field :default_descent_league_uuid, :string
    field :scoring_config, :scoring_config
  end

  object :match_context do
    field :league_group_ref, :string
    field :league_ref, :string
    field :season_ref, :string
    field :match_day_ref, :string
    field :match_ref, :string
    field :location_ref, :string
    field :home_team_ref, :string
    field :guest_team_ref, :string
    field :tenant_ref, :string
    field :matchday_round, :string
    field :league_group_name, :string
    field :league_name, :string
    field :season_name, :string
    field :guest_team_name, :string
    field :home_team_name, :string
    field :location_name, :string
    field :date_time, :naive_datetime
    field :league_level, :integer
    field :season_start_date, :date
    field :season_end_date, :date
    field :match_comment, :string
    field :orga_comment, :string
    field :match_day_number, :integer
    field :config, :config
    field :home_players, list_of(:player)
    field :guest_players, list_of(:player)
  end
end
