defmodule RecorderWeb.Resolvers.Matches do
  alias Recorder.Dart.Matches

  def list_posts(_parent, %{uuid: uuid}, _resolution) do
    {:ok, Matches.match_context_by_uuid(uuid)}
  end
end
