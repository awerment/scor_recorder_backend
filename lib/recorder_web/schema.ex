defmodule RecorderWeb.Schema do
  use Absinthe.Schema
  import_types RecorderWeb.Schema.ContentTypes

  alias RecorderWeb.Resolvers

  query do
    @desc "Get a match context by the match uuid"
    field :match_context, :match_context do
      arg :uuid, non_null(:string)
      resolve &Resolvers.Matches.list_posts/3
    end
  end
end
