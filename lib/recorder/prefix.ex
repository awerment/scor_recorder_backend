defmodule Recorder.Prefix do
  def get(:dart), do: "scor_dart_hdvev_735bb4"
  def get(:global), do: "scor_global_hdvev_735bb4"
end
