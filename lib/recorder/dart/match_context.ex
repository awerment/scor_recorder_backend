defmodule Recorder.Dart.MatchContext do
  use Ecto.Schema

  schema "dart_match_context" do
    field :league_group_ref
    field :league_ref
    field :season_ref
    field :match_day_ref
    field :match_ref
    field :location_ref
    field :home_team_ref
    field :guest_team_ref
    field :tenant_ref
    field :matchday_round
    field :league_group_name
    field :league_name
    field :season_name
    field :guest_team_name
    field :home_team_name
    field :location_name
    field :date_time, :naive_datetime
    field :league_level, :integer
    field :season_start_date, :date
    field :season_end_date, :date
    field :match_comment
    field :orga_comment
    field :match_day_number, :integer
    field :home_players_raw, :string, source: :home_players
    field :guest_players_raw, :string, source: :guest_players
    field :config_raw, :string, source: :config
    field :config, :any, virtual: true
    field :home_players, :any, virtual: true
    field :guest_players, :any, virtual: true
  end
end
