defmodule Recorder.Dart.SeasonConfig.BorrowConfig do
  use Ecto.Schema

  @primary_key false
  embedded_schema do
    field :borrow_max_count, :integer, source: :borrowMaxCount
    field :borrow_direction, :string, source: :borrowDirection
    field :borrow_interval, :string, source: :borrowInterval
  end
end
