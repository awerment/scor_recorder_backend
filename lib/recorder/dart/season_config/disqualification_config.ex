defmodule Recorder.Dart.SeasonConfig.DisqualificationConfig do
  use Ecto.Schema

  @primary_key false
  embedded_schema do
    field :consider_in_table, :boolean, source: :considerInTable
    field :consider_in_player_statistics, :boolean, source: :considerInPlayerStatistics
    field :consider_in_team_statistics, :boolean, source: :considerInTeamStatistics
  end
end
