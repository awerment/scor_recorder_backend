defmodule Recorder.Dart.SeasonConfig.ScoringConfig do
  use Ecto.Schema

  @primary_key false
  embedded_schema do
    field :is_draw_allowed, :boolean, default: true, source: :isDrawAllowed
    field :winning_points, :integer, default: 2, source: :winningPoints
    field :losing_points, :integer, default: 0, source: :losingPoints
    field :draw_points, :integer, default: 1, source: :drawPoints
    field :deciding_match_points, :integer, default: 0, source: :decidingMatchPoints
  end
end
