defmodule Recorder.Dart.SeasonConfig.RangeConfig do
  use Ecto.Schema

  @primary_key false
  embedded_schema do
    field :upper_bound, :integer, source: :upperBound
    field :lower_bound, :integer, source: :lowerBound
  end
end
