defmodule Recorder.Dart.SeasonConfig.GameConfig do
  use Ecto.Schema

  @primary_key false
  embedded_schema do
    field :game_count, :integer, source: :gameCount
    field :game_distance, :integer, source: :gameDistance
    field :set_distance, :integer, source: :setDistance
    field :leg_distance, :integer, source: :legDistance
    field :shortleg, :integer, source: :shortleg
    field :capture_shortleg, :boolean, source: :captureShortleg
    field :capture_highfinish, :boolean, source: :captureHighfinish
    field :capture_one_eighty, :boolean, source: :captureOneEighty
    field :capture_one_seventy_one, :boolean, source: :captureOneSeventyOne
    field :capture_average, :boolean, source: :captureAverage
    field :highfinish, :integer, source: :highfinish
    field :starting_team, :string, source: :startingTeam
    field :begin_games, :string, source: :beginGames
  end
end
