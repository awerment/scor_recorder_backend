defmodule Recorder.Dart.Matches do
  alias Recorder.DartRepo
  alias Recorder.Dart.{MatchContext, SeasonConfig, Player}
  alias Recorder.Prefix

  def match_context_by_uuid(match_ref, opts \\ []) when is_binary(match_ref) do
    prefix = opts[:prefix] || Prefix.get(:dart)
    with result <- DartRepo.get_by(MatchContext, [match_ref: match_ref], prefix: prefix),
         maybe_parsed_config <- try_decode(result.config_raw, SeasonConfig),
         maybe_parsed_home_players <- try_decode(result.home_players_raw, Player),
         maybe_parsed_guest_players <- try_decode(result.guest_players_raw, Player) do
      Map.merge(result, %{
        config: maybe_parsed_config,
        home_players: maybe_parsed_home_players,
        guest_players: maybe_parsed_guest_players,
        config_raw: nil,
        home_players_raw: nil,
        guest_players_raw: nil
      })
    end
  end

  defp try_decode(json_string, mod) when is_binary(json_string) do
    case Jason.decode(json_string) do
      {:ok, json} when is_map(json) -> DartRepo.load(mod, json)
      {:ok, json} when is_list(json) -> Enum.map(json, &DartRepo.load(mod, &1))
      {:error, _} -> nil
    end
  end

end
