defmodule Recorder.Dart.SeasonConfig do
  use Ecto.Schema

  @primary_key false
  embedded_schema do
    field :version, :integer
    field :rounds, :integer
    field :name
    field :use_season_report, :boolean, default: false, source: :useSeasonReport
    field :is_draw_allowed, :boolean, default: true, source: :isDrawAllowed
    field :default_match_start_time, :date, source: :defaultMatchStartTime
    embeds_one :single_config, Recorder.Dart.SeasonConfig.GameConfig, source: :singleConfig
    embeds_one :double_config, Recorder.Dart.SeasonConfig.GameConfig, source: :doubleConfig
    embeds_one :disqualify_config, Recorder.Dart.SeasonConfig.DisqualificationConfig, source: :disqualifyConfig
    embeds_one :borrow_config, Recorder.Dart.SeasonConfig.BorrowConfig, source: :borrowConfig
    embeds_one :rank_ascent_config, Recorder.Dart.SeasonConfig.RangeConfig, source: :rankAscentConfig
    embeds_one :rank_descent_config, Recorder.Dart.SeasonConfig.RangeConfig, source: :rankDescentConfig
    embeds_one :rank_neutral_config, Recorder.Dart.SeasonConfig.RangeConfig, source: :rankNeutralConfig
    embeds_one :relegation_ascent_config, Recorder.Dart.SeasonConfig.RangeConfig, source: :relegationAscentConfig
    embeds_one :relegation_descent_config, Recorder.Dart.SeasonConfig.RangeConfig, source: :relegationDescentConfig
    field :default_ascent_league_uuid, :string, source: :defaultAscentLeagueUuid
    field :default_descent_league_uuid, :string, source: :defaultDescentLeagueUuid
    embeds_one :scoring_config, Recorder.Dart.SeasonConfig.ScoringConfig, source: :scoringConfig
  end
end
