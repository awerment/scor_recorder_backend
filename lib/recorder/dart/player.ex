defmodule Recorder.Dart.Player do
  use Ecto.Schema

  @primary_key false
  embedded_schema do
    field :login
    field :uuid
    field :state
    field :pass_number, :string, source: :passNumber
    field :first_name, :string, source: :firstName
    field :last_name, :string, source: :lastName
    field :comment
    field :user_ref, :string, source: :userRef
    field :nation
    field :date_of_birth, :date, source: :dateOfBirth
    field :contact_data_ref, :string, source: :contactDataRef
    field :created_by, :string, source: :createdBy
    field :created_date, :date, source: :createdDate
    field :last_modified_by, :string, source: :lastModifiedBy
    field :last_modified_date, :date, source: :lastModifiedDate
    field :password
    field :email
  end
end
