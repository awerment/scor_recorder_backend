defmodule Recorder.DartRepo do
  use Ecto.Repo,
    otp_app: :recorder,
    adapter: Ecto.Adapters.Postgres
end
